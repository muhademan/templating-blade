<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/data-tables', function () {
    return view('items.create');
});



Route::get('/pertanyaan', 'App\Http\Controllers\PertanyaanController@index');
Route::get('/pertanyaan/create', 'App\Http\Controllers\PertanyaanController@create');
Route::post('/pertanyaan', 'App\Http\Controllers\PertanyaanController@store');
Route::get('/pertanyaan/{id}', 'App\Http\Controllers\PertanyaanController@show');
Route::get('/pertanyaan/{id}/edit', 'App\Http\Controllers\PertanyaanController@edit');
Route::put('/pertanyaan/{id}', 'App\Http\Controllers\PertanyaanController@update');
Route::delete('/pertanyaan/{id}', 'App\Http\Controllers\PertanyaanController@destroy');

// route post
// Route::resource('post', 'PostController');
Route::get('/post', 'App\Http\Controllers\PostController@index');
Route::get('/post/create', 'App\Http\Controllers\PostController@create');
Route::post('/post', 'App\Http\Controllers\PostController@store');
Route::get('/post/{id}', 'App\Http\Controllers\PostController@show');
Route::get('/post/{id}/edit', 'App\Http\Controllers\PostController@edit');
Route::put('/post/{id}', 'App\Http\Controllers\PostController@update');
Route::delete('/post/{id}', 'App\Http\Controllers\PostController@destroy');
// Route::get('/master', function () {
//     return view('adminlte/master');
// });

// Route::get('/items', function () {
//     return view('items.index');
// });

// Route::get('/items/create', function () {
//     return view('items.create');
// });
