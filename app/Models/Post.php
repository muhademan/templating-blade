<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $timestamps = false; // untuk menghilangkan coloumn update_at dan juga create_at
    protected $table = "post";
    protected $fillable = ["title", "body"];
}
