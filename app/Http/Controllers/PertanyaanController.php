<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PertanyaanController extends Controller
{
    public function create()
    {
        return view('pertanyaan.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required',
            'tanggal_dibuat' => 'required',
            'tanggal_diperbaharui' => 'required',
            'jawaban_tepat_id' => 'required',
            'profil_id' => 'required',
        ]);
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"],
            "tanggal_dibuat" => $request["tanggal_dibuat"],
            "tanggal_diperbaharui" => $request["tanggal_diperbaharui"],
            "jawaban_tepat_id" => $request["jawaban_tepat_id"],
            "profil_id" => $request["profil_id"]
        ]);
        return redirect('/pertanyaan');
    }

    public function index()
    {
        $post = DB::table('pertanyaan')->get();
        return view('pertanyaan.index', compact('post'));
    }

    public function show($id)
    {
        $post = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.show', compact('post'));
    }

    public function edit($id)
    {
        $post = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required',
            'tanggal_dibuat' => 'required',
            'tanggal_diperbaharui' => 'required',
            'jawaban_tepat_id' => 'required',
            'profil_id' => 'required',
        ]);

        $query = DB::table('pertanyaan')
            ->where('id', $id)
            ->update([
                "judul" => $request["judul"],
                "isi" => $request["isi"],
                "tanggal_dibuat" => $request["tanggal_dibuat"],
                "tanggal_diperbaharui" => $request["tanggal_diperbaharui"],
                "jawaban_tepat_id" => $request["jawaban_tepat_id"],
                "profil_id" => $request["profil_id"]
            ]);
        return redirect('/pertanyaan');
    }

    public function destroy($id)
    {
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan');
    }
}
