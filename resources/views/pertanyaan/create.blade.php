@extends('adminlte.master');
@section('content');

<div class="card card-primary">
    <h2>Tambah Data</h2>
    <form action="/pertanyaan" method="POST">
        @csrf
        <div class="form-group">
            <label for="title">judul</label>
            <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan judul">
            @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">isi</label>
            <input type="text" class="form-control" name="isi" id="isi" placeholder="Masukkan isi">
            @error('isi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">tanggal dibuat</label>
            <input type="date" class="form-control" name="tanggal_dibuat" id="tanggal_dibuat">
            @error('tanggal_dibuat')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">tanggal diperbaharui</label>
            <input type="date" class="form-control" name="tanggal_diperbaharui" id="tanggal_diperbaharui">
            @error('tanggal_diperbaharui')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">jawaban tepat id</label>
            <input type="number" class="form-control" name="jawaban_tepat_id" id="jawaban_tepat_id" placeholder="Masukkan jawaban tepat id">
            @error('jawaban_tepat_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">profil id</label>
            <input type="number" class="form-control" name="profil_id" id="profil_id" placeholder="Masukkan profil id">
            @error('profil_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>
@endsection