@extends('adminlte.master');
@section('content');

<div class="card card-primary">
    <h2>Edit Post {{$post->id}}</h2>
    <form action="/pertanyaan/{{$post->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="title">Judul</label>
            <input type="text" class="form-control" name="judul" value="{{$post->judul}}" id="judul" placeholder="Masukkan Judul">
            @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Isi</label>
            <input type="text" class="form-control" name="isi" value="{{$post->isi}}" id="isi" placeholder="Masukkan Isi">
            @error('isi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">tanggal dibuat</label>
            <input type="date" class="form-control" name="tanggal_dibuat" value="{{$post->tanggal_dibuat}}" id="tanggal_dibuat">
            @error('tanggal_dibuat')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">tanggal diperbaharui</label>
            <input type="date" class="form-control" name="tanggal_diperbaharui" value="{{$post->tanggal_diperbaharui}}" id="tanggal_diperbaharui">
            @error('tanggal_diperbaharui')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">jawaban tepat id</label>
            <input type="number" class="form-control" name="jawaban_tepat_id" value="{{$post->jawaban_tepat_id}}" id="jawaban_tepat_id" placeholder="Masukkan jawaban tepat id">
            @error('jawaban_tepat_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">profil id</label>
            <input type="number" class="form-control" name="profil_id" value="{{$post->profil_id}}" id="profil_id" placeholder="Masukkan profil id">
            @error('profil_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection