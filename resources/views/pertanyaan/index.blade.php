@extends('adminlte.master');
@section('content');

<div class="card card-primary">
    <a href="/pertanyaan/create" class="btn btn-primary">Tambah</a>
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">judul</th>
                <th scope="col">isi</th>
                <th scope="col">tanggal dibuat</th>
                <th scope="col">tanggal diperbaharui</th>
                <th scope="col">jawaban tepat id</th>
                <th scope="col">profil id</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($post as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->judul}}</td>
                <td>{{$value->isi}}</td>
                <td>{{$value->tanggal_dibuat}}</td>
                <td>{{$value->tanggal_diperbaharui}}</td>
                <td>{{$value->jawaban_tepat_id}}</td>
                <td>{{$value->profil_id}}</td>
                <td>
                    <a href="/pertanyaan/{{$value->id}}" class="btn btn-info">Show</a>
                    <a href="/pertanyaan/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                    <form action="/pertanyaan/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
            @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>
@endsection